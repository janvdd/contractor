FROM python:3.8-slim-buster

ENV PYTHONDONTWRITEBYTECODE=1 PYTHONUNBUFFERED=1

RUN mkdir /code

WORKDIR /code

COPY requirements.txt /tmp/requirements.txt

COPY . /code/

RUN apt-get update && apt-get install -y netcat

RUN mkdir -p /code/static
RUN pip install -r /tmp/requirements.txt

