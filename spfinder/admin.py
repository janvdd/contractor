from django.contrib import admin

# Register your models here.
from spfinder.models import RegistrationGroup

admin.site.register(RegistrationGroup)
