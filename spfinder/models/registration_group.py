from django.db import models


class RegistrationGroup(models.Model):
    created_at = models.DateTimeField(auto_now=False, auto_now_add=True)
    is_active = models.BooleanField(default=True)

    name = models.CharField(max_length=100, unique=True)
