from django.db import models


class ServiceProvider(models.Model):
    created_at = models.DateTimeField(auto_now=False, auto_now_add=True)
    is_active = models.BooleanField(default=True)

    name = models.CharField(max_length=100)
    email = models.EmailField(null=True)
    phone = models.CharField(max_length=12, null=True)
    postcode = models.CharField(max_length=4, null=True)

    website = models.CharField(max_length=100, null=True)

    registration_groups = models.ManyToManyField('spfinder.RegistrationGroup',
                                                 related_name='spfinder')
