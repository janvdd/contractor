from django.db import models

class Suburb(models.Model):
    name = models.CharField(max_length=200, unique=True)
    postcode = models.IntegerField()
    longitude = models.FloatField(null=True, blank=True)  # point?
    latitude = models.FloatField(null=True, blank=True) # point?



