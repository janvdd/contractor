from django.urls import path

from spfinder.views import *

urlpatterns = [
    # allows searching for providers
    path('', ProviderSearchView.as_view()),

    # allows viewing of provider details
    path('provider/<str:provider-name>/', ProviderView.as_view()),

    # allows posting + viewing of notes user has posted against provider
    path('provider/<str:provicer-name>/notes/', ProviderNoteView.as_view()),

    # allows posting + viewing of interactions against provideer
    path('provider/<str:provicer-name>/interactions/', ProviderContactView.as_view()),

    # shows a list of all notes + interactions with providers
    path('history', ProviderHistoryView.as_view()),

    # return list of registration groups
    path('registration-groups/', RegistrationGroupView.as_view())

]
