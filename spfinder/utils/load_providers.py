"""
Loads in data to database. Make sure the first row is deleted so columns
work properly.

Requires setting of DJANGO_SETTINGS_MODULE in env vars
"""

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

import pandas as pd

from ..models import ServiceProvider, RegistrationGroup, Suburb

NUM_TO_IMPORT = 1000

fp = r'C:\Users\jan\Downloads\PB Provider lists by name XLS.xlsx'

df = pd.read_excel(fp, sheet_name='Provider_by_Name_VIC')

rgs = df['Registration_Group'].drop_duplicates().values.tolist()

db_rgs = RegistrationGroup.objects.all()

cols = ['Registered_Provider_Name',
        'PrvdrPhnNmbr',
        'Website',
        'Email_Address',
        'PostCd']

ff = df.copy()

ff = ff[cols]

ff['Registered_Provider_Name'].fillna('', inplace=True)
ff['PrvdrPhnNmbr'].fillna('', inplace=True)
ff['Website'].fillna('', inplace=True)
ff['Email_Address'].fillna('', inplace=True)
ff['PostCd'].fillna(-1, inplace=True)

ff.drop_duplicates(inplace=True)

for i, row in ff.iterrows():
    if i == NUM_TO_IMPORT:
        break
    name = row['Registered_Provider_Name']
    if name == '':
        continue

    phone = row['PrvdrPhnNmbr'] if row['PrvdrPhnNmbr'] is not '' else None
    website = row['Website'] if row['Website'] is not '' else None
    email = row['Email_Address'] if row['Email_Address'] is not '' else None
    postcode = int(row['PostCd']) if row['PostCd'] is not -1 else None

    postcode_obj = Suburb.objects.get(value=postcode)

    sp = ServiceProvider.objects.create(
        name=name,
        phone=phone,
        website=website,
        email=email,
        postcode=postcode
    )

    rgs = df.loc[df['Registered_Provider_Name'] == name]['Registration_Group'].drop_duplicates().values.tolist()
    rgs_objs = db_rgs.filter(name__in=rgs).all()
    for r in rgs_objs:
        sp.registration_groups.add(r)
