from rest_framework.views import APIView


class ProviderContactView(APIView):
    """
    Allows user to 'interact' with the provider by using the creating an interaction object that
    will send an email
    """

    def get(self, request):
        """
        Returns a paginated list of interactions with the provider
        :param request:
        :return:
        """
        ...

    def post(self, request):
        """
        Allows the logged in user to post an interaction against the provider
        :param request:
        :return:
        """
