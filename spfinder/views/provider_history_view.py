from rest_framework.views import APIView


class ProviderHistoryView(APIView):
    def get(self, request):
        """
        Returns a paginated list of all interactions (contacts/notes)

        Filtering:
            Type (contact/note)
            Time period
            Registration Group
            Provider Name
        :param request:
        :return:
        """
        ...
