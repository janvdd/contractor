from rest_framework.views import APIView


class ProviderNoteView(APIView):
    """
    Returns all of the notes for the logged in user that the user has posted against the provider,
    and allows posting of notes
    """

    def get(self, request):
        """
        Returns a paginated list of notes/interactions with the provider
        :param request:
        :return:
        """
        ...

    def post(self, request):
        """
        Allows the logged in user to post a note against the provider
        :param request:
        :return:
        """
