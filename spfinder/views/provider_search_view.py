from rest_framework.views import APIView


class ProviderSearchView(APIView):
    """
    Should return a paginated list of providers and accept a list of args that allow filtering.

    Args:
        pageSize: size of page
        pageNum: page number to return
        postCode: centrepoint
        radius: distance from postCode centrepoint to show providers
        registrationGroup: attribute
        providerName: name of provider
    """
    ...
