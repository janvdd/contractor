from rest_framework.views import APIView


class ProviderView(APIView):
    """
    Should return details of the provider
    """

    def get(self, request):
        """
        Returns an object representing the provider
        :param request:
        :return:
        """
        ...


