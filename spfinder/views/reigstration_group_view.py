from rest_framework.views import APIView


class RegistrationGroupView(APIView):
    def get(self, request):
        """
        Returns all registration groups
        :param request:
        :return:
        """
        ...


