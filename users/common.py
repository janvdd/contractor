from rest_framework_simplejwt.tokens import RefreshToken

from .models import User


def get_new_token(user: User) -> dict:
    """
    Manually generates a new token access and refresh token
    for a given user using simple JWT.

    :param user:
    :return:
    """

    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }
