from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
    user_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if email is None:
            raise ValueError('The given email or username must be set')

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', False)

        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password):
        user = self.model(
            email=email,
            is_superuser=True,
        )
        user.set_password(password)
        user.save(using=self._db)

        return user

