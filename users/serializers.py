from rest_framework import serializers

from users.models import Login


class LoginWebSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=100)
    password = serializers.CharField(min_length=4, max_length=100)

    class Meta:
        model = Login
        fields = ['email', 'password']
