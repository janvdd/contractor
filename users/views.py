import logging

from django.contrib.auth import authenticate
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from users.serializers import LoginWebSerializer
from . import common

logger = logging.getLogger(__name__)


class LoginView(APIView):
    permission_classes = []

    def post(self, request) -> Response:
        serializer = LoginWebSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = serializer.validated_data['email']
        pwd = serializer.validated_data['password']

        user = authenticate(username=email, password=pwd)

        if user is None:
            msg = "This username and password combination doesn't exist in our records."
            return Response({'error': msg}, status=status.HTTP_401_UNAUTHORIZED)

        token_dict = common.get_new_token(user)

        json_ = {
            'token': token_dict['access'],
            'token_refresh': token_dict['refresh'],
        }

        return Response(json_, status.HTTP_200_OK)
